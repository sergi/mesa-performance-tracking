---
- name: &dashboard-title Mesa driver performance
  project:
    dashboards:
    - &dashboard-id mesa-driver-performance-v2

- name: *dashboard-id
  dashboard:
    title: *dashboard-title
    time_options: [1h, 6h, 12h, 24h, 2d, 7d, 14d, 30d]
    refresh_intervals: [5m, 15m, 30m, 1h]
    time:
      from: now-30d
      to: now
    templates:
    - query:
        query: 'import "influxdata/influxdb/schema"
                import "strings"
                schema.tagValues(
                  bucket: "mesa-perf-v2",
                  tag: "job_name",
                )'
        name: job
        label: Job
        datasource: &datasource InfluxDB-CI-Stats-v2
    - query:
        query: 'import "influxdata/influxdb/schema"
                schema.tagValues(
                  bucket: "mesa-perf-v2",
                  tag: "trace_name",
                  predicate: (r) => true,
                )'
        name: trace
        label: Trace
        datasource: *datasource
        includeAll: true
        multi: true
    sharedCrosshair: true
    rows:
    - row:
        panels:
        - graph:
            title: "${trace}"
            repeat: trace
            yaxes:
                # Left y-axis
                - yaxis:
                    format: short
                    min: 0
                    label: "FPS"
                # Right y-axis
                - yaxis:
                    show: false
            legend:
              show: false
            pointradius: 2
            points: true
            datasource: *datasource
            targets:
                - influxdb-target:
                    # NOTE: The query will be formatted regarding Python string
                    # format method, so influxdb expressions using '{' and '}'
                    # characters must be escaped.
                    query: |
                      import "influxdata/influxdb/schema"

                      ns_to_fps = (tables=<-) =>
                        tables
                          |> map(fn: (r) => ({{
                            r with
                            frame_time:
                              float(v: 1000000000 / r.frame_time)
                          }}))

                      from(bucket: "mesa-perf-v2")
                        |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
                        |> filter(fn:(r) =>
                          r._measurement == "frame_time" and
                          r.job_name == "${job}" and
                          r.trace_name == "${trace}"
                        )
                        |> schema.fieldsAsCols()
                        |> group(
                          columns: ["_measurement", "job_id", "pipeline_id", "commit_sha", "commit_title"],
                          mode:"by"
                        )
                        |> ns_to_fps()
                        |> rename(columns: {{frame_time: "_value"}})

                        |> window(every: 1m)
                        |> median(method: "exact_mean")
                        |> duplicate(column: "_stop", as: "_time")
                        |> window(every: inf)