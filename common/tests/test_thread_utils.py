import time

import pytest

from common.thread_utils import SmartThreadPoolExecutor


def square(x):
    return x**2


def divide(x):
    return 1 / x


def pow_two_three(x, y):
    return pow(x, y)


def long_running_job(before_ms=0, func=None, after_ms=0):
    time.sleep(before_ms / 1000)
    if func is not None:
        func()
    time.sleep(after_ms / 1000)


def test_map():
    # Test that map is working correctly
    with SmartThreadPoolExecutor() as executor:
        results = list(executor.map(square, range(10)))
    # results would come out of order if we were using threads
    sorted_results = sorted(results)
    assert len(results) == 10
    assert sorted_results == [x**2 for x in range(10)]


def test_exception():
    # Test that an exception in a worker function is raised
    with pytest.raises(ZeroDivisionError):
        with SmartThreadPoolExecutor() as executor:
            list(executor.map(divide, [0]))


def test_multi_args_map():
    # Test that map handles multiple arguments correctly
    with SmartThreadPoolExecutor() as executor:
        results = list(executor.map(pow_two_three, [2] * 10, range(10)))
    sorted_results = sorted(results)
    assert len(results) == 10
    assert sorted_results == [pow(2, x) for x in range(10)]


# use freezegun to mock time
def test_timeout():
    # Test that an exception in a worker function is raised
    with pytest.raises(TimeoutError):
        with SmartThreadPoolExecutor() as executor:
            list(executor.map(long_running_job, range(100), timeout=0.05))
